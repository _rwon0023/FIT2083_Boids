// note mathjs -> math.eigs gives unitary vectors

var neighbourAmt = 1;
var stepInterval = 500;//30;//1;//33;
var boidCount =80//781;
var meterScale = 10;
var boidSize = 1*meterScale; 
var boidSpeed = 5*meterScale;
var distFromObstacle = 150*meterScale;
var boidMaxVision = 60*meterScale;
var boidSepDist = 10*meterScale;
var envDimensionsH = 900;
var envDimensionsW = 1500;
var boidFOVdeg = 148;

var sdafs = true;//enable obs

var pauseStep = 100

var radiuhhh = Math.PI/60; //Math.PI / 60;

//var sepVsCohWeight = 0.01;

var cohW = 2;
var sepW = 1;

var nearNeighbour_n = 13;

var distanceToNearestNeighbour = {}

var connectedComponentTrackThing = {}
var connectedComponentTrackThing_n = 1000;

var currentStep = 0;
var envDimensionsMeters = [envDimensionsH/meterScale, envDimensionsW/meterScale];

var debugAngle = false; function toggleDebugAngle(){debugAngle=!debugAngle}
var matchDebugColours = false;
var stopSim = false; function pausePlaySim(){stopSim=!stopSim}

var randness = 20*meterScale;

function sparseness(){return (averageOfHashmapValues(distanceToNearestNeighbour))*((nearNeighbour_n+1)**(1/3));}

function getAvgDist(){
    _distanceToNearestNeighbour = [...distanceToNearestNeighbour]
    for (let i = 0; i < distanceToNearestNeighbour.length; i++) {
        _distanceToNearestNeighbour[i] /= boidCount;
        _distanceToNearestNeighbour[i]/=meterScale;
    }
    return _distanceToNearestNeighbour;
}

function hash(obj) {

    return obj.id
  
  }

function averageOfHashmapValues(thaMap){
    var vals = Object.values(thaMap)
    const sumAll = vals.reduce((a, b) => a + b, 0);
    const avgAll = (sumAll / vals.length) || 0;
    return avgAll;
}
const delay = ms => new Promise(res => setTimeout(res, ms));

function Graph(){
    this.verts = []
    this.adjList = {}
}


Graph.prototype.noEdge = function(u,v){
    return !(u in this.adjList && (this.adjList[u].includes(v)))
}

Graph.prototype.addDirEdge = function(u,v){
    if(this.noEdge(hash(u),v)){
        if(!this.verts.includes(u)) this.verts.push(u)
        if(!this.verts.includes(v)) this.verts.push(v)

        if(!(hash(u) in this.adjList)){
            this.adjList[hash(u)] = []
        }
        this.adjList[hash(u)].push(v)
    }
}

Graph.prototype.addUndirEdge = function(u,v){
    this.addDirEdge(u,v)
    this.addDirEdge(v,u)
}

Graph.prototype.getGraphConnectedComponents = function(){
    var cc = []
    var visited = {};
    var unvisited = [...this.verts]
    
    this.verts.forEach((vert)=>{visited[hash(vert)] = false})

    while(unvisited.length>0){
        var component = []
        let stack = [unvisited[0]];

        while (stack.length != 0){
            s = stack.pop();
            sh = hash(s)
            if (visited[sh] == false)
            {
                component.push(s)
                unvisited = unvisited.filter(e=>e!=s);
                visited[sh] = true;
            }
            for(let node = 0; node < this.adjList[sh].length; node++){
                n = this.adjList[sh][node]
                if(!visited[hash(n)]){
                    stack.push(n)
                }
            }
        }

        cc.push(component);

    }

    /*while(visited.length != this.verts.length){
        if(current.length > 0){
            var vert = current.pop()
            if(!visited.includes(vert)){
                visited.push(vert)
                component.push(vert)
                this.adjList[hash(vert)].forEach((neigh)=>{
                    current.push(neigh)
                });
            }
        }else{
            cc.push([...component])
            component = []
            current.push(this.verts.find(e=>!(visited.includes(e))))
        }
    }*/

    return cc
}


function getConnectedComponents(swarmRef, drawLines = false){
    stopSim = true;
    var undirGraph = new Graph()
    var boidsUnconnected = [...swarmRef.boids];
    var currBoids = [];
    var currBoid = boidsUnconnected[0];
    while(boidsUnconnected.length > 0) {
        
        boidsUnconnected = boidsUnconnected.filter(e=>e!=currBoid);
        
        if(currBoid.latestNeighbours.length >= 3){
            for (let i = 0; i < 3; i++) {
                var neigh = currBoid.latestNeighbours[i];
                
                if(Obstacle.prototype.isPrototypeOf(neigh)){
                    continue
                }

                if(drawLines){
                    debugAngle = true
                    matchDebugColours = true
                    currBoid.angle(neigh,envDimensionsW,envDimensionsH)
                    debugAngle = false;
                    matchDebugColours = false;
                }
                if(boidsUnconnected.includes(neigh)){
                    currBoids.push(neigh);
                }
                undirGraph.addUndirEdge(currBoid,neigh)
            }
        }
        
        if(currBoids.length>0){
            currBoid = currBoids.pop();
        }else{
            currBoid = boidsUnconnected[0];
        }
    }
    var out = undirGraph.getGraphConnectedComponents();
    //console.log(out)
    return out.length;
}

function waitUntilComponentsConnected(){
    var uh =getConnectedComponents(swarm)
    if(uh != 1) {
        stopSim = false;
        window.setTimeout(()=>{console.log(waitUntilComponentsConnected())}, 100); /* this checks the flag every 100 milliseconds*/
     }else{
        return getConnectedComponents(swarm, true)
     }
     return uh
}


function getAnisotropy(n){
    var m = []
    for (let i = 0; i < swarm.boids.length; i++) {
        var currBoid = swarm.boids[i];
        if(currBoid.latestNeighbours.length > n){
            var neigh = currBoid.latestNeighbours[n]
            var uvector = currBoid.unitVector(neigh, envDimensionsW,envDimensionsH)
            var val = uvector[0]*uvector[1]
            var x = Math.round(currBoid.x);
            var y = Math.round(currBoid.y);

            if(m[x] == undefined){
                m[x] = []
            }
            if(m[x][y] == undefined){
                m[x][y] = 0
            }
            m[x][y] += val
        }
    }

    var widest = m.length;
    for (let row = 0; row < m.length; row++) {
        if(m[row] != undefined){
            if(m[row].length > widest){
                widest = m[row].length;
            }
        }
    }

    for (let row = 0; row < widest; row++) {
        if(m[row] == undefined) m[row] = []
        for (let col = 0; col < widest; col++) {
            if(m[row][col] == undefined){
                m[row][col] = 0
            }else{
                m[row][col] /= boidCount;
            }
        }
    }


    return m
}


var trackedConnectedComponents = []
function trackComponentsForSteps(duration,step=100){
    trackedConnectedComponents = []
    while (duration>0){
        window.setTimeout(()=>{
            var ccs = getConnectedComponents(swarm,true);
            trackedConnectedComponents.push(ccs);
            console.log(ccs);
            stopSim = false;
        }, duration);
        duration-=step
    }
}


function Obstacle(){
    this.x = envDimensionsW/3
    this.y = envDimensionsH/2 + randness/2
    this.heading = Math.PI
    this.enabled = sdafs
}
Obstacle.prototype.radius = 2*meterScale/2
Obstacle.prototype.colour = 'black'

Obstacle.prototype.draw = function(ctx){
    var pointLen = this.radius * 2.5;
    ctx.fillStyle = (this.enabled)?this.colour:'red';
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(this.x + Math.cos(this.heading + Math.PI / 2) * this.radius,
               this.y + Math.sin(this.heading + Math.PI / 2) * this.radius);
    ctx.lineTo(this.x - Math.cos(this.heading + Math.PI) * pointLen,
               this.y - Math.sin(this.heading + Math.PI) * pointLen);
    ctx.lineTo(this.x + Math.cos(this.heading - Math.PI / 2) * this.radius,
               this.y + Math.sin(this.heading - Math.PI / 2) * this.radius);
    ctx.fill();
}

var obstacleInstance = new Obstacle()

/* Boid prototype */
var idIterator = 0
function Boid(swarm) {
    this.id = ++idIterator;
    this.x = Math.random() * swarm.width;
    this.y = Math.random() * swarm.height;
    this.heading = Math.random() * 2 * Math.PI - Math.PI;
    this.hsl = [360 * Math.random(),0.6,0.5];
    this.colour = 'hsl(' + this.hsl[0] + ', '+this.hsl[1]*100+'%, '+this.hsl[2]*100+'%)';
    this.latestNeighbours = [];
}

Boid.prototype.radius = boidSize/2;
Boid.prototype.speed = boidSpeed;
Boid.prototype.radialSpeed = radiuhhh//Math.PI / 60;
Boid.prototype.vision = boidMaxVision;

Boid.prototype.draw = function(ctx) {
    var pointLen = this.radius * 2.5;
    ctx.fillStyle = this.colour;
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(this.x + Math.cos(this.heading + Math.PI / 2) * this.radius,
               this.y + Math.sin(this.heading + Math.PI / 2) * this.radius);
    ctx.lineTo(this.x - Math.cos(this.heading + Math.PI) * pointLen,
               this.y - Math.sin(this.heading + Math.PI) * pointLen);
    ctx.lineTo(this.x + Math.cos(this.heading - Math.PI / 2) * this.radius,
               this.y + Math.sin(this.heading - Math.PI / 2) * this.radius);
    ctx.fill();
};

Boid.prototype.distance = function(boid, width, height) {
    var x0 = Math.min(this.x, boid.x), x1 = Math.max(this.x, boid.x);
    var y0 = Math.min(this.y, boid.y), y1 = Math.max(this.y, boid.y);
    var dx = Math.min(x1 - x0, x0 + width - x1);
    var dy = Math.min(y1 - y0, y0 + height - y1);
    return Math.sqrt(dx * dx + dy * dy);
};
var ctxLeak;
Boid.prototype.angle = function(boid, width, height) {
    var x0 = Math.min(this.x, boid.x), x1 = Math.max(this.x, boid.x);
    var y0 = Math.min(this.y, boid.y), y1 = Math.max(this.y, boid.y);
    var dx = Math.min(x1 - x0, x0 + width - x1);
    var dy = Math.min(y1 - y0, y0 + height - y1);

    if(x1 == this.x){
        if(dx == x1 - x0){
            dx *= -1;
        }
    }else{
        if(dx == x0 + width - x1){
            dx *= -1;
        }
    }
    if(y1 == this.y){
        if(dy == y1 - y0){
            dy *= -1;
        }
    }else{
        if(dy == y0 + height - y1){
            dy *= -1;
        }
    }

    var hx = Math.cos(this.heading), hy = Math.sin(this.heading);
    var dot = hx*dx + hy*dy;
    var magD = Math.sqrt(dx * dx + dy * dy);
    var magH = Math.sqrt(hx * hx + hy * hy);

    if(debugAngle){
        ctxLeak.strokeStyle = 'rgba(255, 0, 0, 0.2)'//'red';
        
        if(matchDebugColours){
            var hslValues = this.hsl
            var rgbValues = hsl2rgb(...hslValues)
            ctxLeak.strokeStyle='rgba('+rgbValues[0]+', '+rgbValues[1]+', '+rgbValues[2]+', 0.2)'
        }

        ctxLeak.beginPath()
        ctxLeak.moveTo(this.x,this.y)
        ctxLeak.lineTo(this.x+dx,this.y+dy)
        ctxLeak.stroke()
        ctxLeak.strokeStyle = 'blue';
        ctxLeak.beginPath()
        ctxLeak.moveTo(this.x,this.y)
        ctxLeak.lineTo(this.x+(hx*this.radius*1.5),this.y+(hy*this.radius*1.5))
        ctxLeak.stroke()
    }
    

    var outangle = Math.acos(dot/Math.abs(magD*magH)) * (180/Math.PI);
    //if(dot<0)outangle= -outangle;
    
    return outangle;
};

Boid.prototype.unitVector = function(boid, width, height) {
    var x0 = Math.min(this.x, boid.x), x1 = Math.max(this.x, boid.x);
    var y0 = Math.min(this.y, boid.y), y1 = Math.max(this.y, boid.y);
    var dx = Math.min(x1 - x0, x0 + width - x1);
    var dy = Math.min(y1 - y0, y0 + height - y1);

    if(x1 == this.x){
        if(dx == x1 - x0){
            dx *= -1;
        }
    }else{
        if(dx == x0 + width - x1){
            dx *= -1;
        }
    }
    if(y1 == this.y){
        if(dy == y1 - y0){
            dy *= -1;
        }
    }else{
        if(dy == y0 + height - y1){
            dy *= -1;
        }
    }
    var mag = Math.sqrt(dx * dx + dy * dy);

    return [dx/mag,dy/mag];
};

Boid.prototype.getNeighbors = function(swarm) {
    var w = swarm.width, h = swarm.height;
    var neighbors = [];
    for (var i = 0; i < swarm.boids.length + 1; i++) {
        var boid
        if(i >= swarm.boids.length){
            boid = obstacleInstance
        }else{
            boid = swarm.boids[i];
        }
        var ob;
        var dist = this.distance(boid, w, h)
        if (this !== boid && dist < this.vision) {
            var angleFromBoid = this.angle(boid, w, h)
            var mindist = this.radius;
            if(dist < mindist || angleFromBoid < boidFOVdeg){
                /*
                if(neighbors.length >= Math.max(nearNeighbour_n, neighbourAmt,3)){
                    var lastTracked = neighbors[neighbors.length-1];
                    if(dist < lastTracked.d || (Obstacle.prototype.isPrototypeOf(boid) && boid.enabled)){
                        if(Obstacle.prototype.isPrototypeOf(boid)){
                            ob = {d:dist, b:boid}
                        }else{
                            neighbors[neighbourAmt-1] = {d:dist,b:boid};
                        }
                    }
                }else{
                    neighbors.push({d:dist,b:boid});
                }
                neighbors.sort((a,b)=>{return a.d-b.d});
                */
                if(Obstacle.prototype.isPrototypeOf(boid) && obstacleInstance.enabled){
                    ob = {d:dist, b:boid}
                }else{
                    neighbors.push({d:dist,b:boid}); //
                }
                
            }
        }
    }
    neighbors.sort((a,b)=>{return a.d-b.d});

    //console.log(neighbors);
    for (let i = 0; i < Math.min(nearNeighbour_n,neighbors.length); i++) {
        if(distanceToNearestNeighbour[i] == undefined) distanceToNearestNeighbour[i] = 0;
        distanceToNearestNeighbour[i] += neighbors[i].d
    }

    /*
    if (neighbors.length>nearNeighbour_n) {
        distanceToNearestNeighbour[hash(neighbors[nearNeighbour_n].b)] = neighbors[nearNeighbour_n].d/meterScale;//hashObject(neighbors[nearNeighbour_n].b)] = neighbors[nearNeighbour_n].d/meterScale;
    }
    */
    //if(neighbors.length > neighbourAmt){
    //    if(ob != undefined && ob.d>neighbors[neighbourAmt].d){
    //        ob = undefined;
    //    }
    //}
    
    

    var out = [];
    neighbors.forEach(e=>out.push(e.b));

    this.latestNeighbours = [...out];

    while (out.length>neighbourAmt){
        out.pop()
    }

    if(ob != undefined){
        out.push(ob.b);
    }

    return out;
};

Boid.wrap = function(value) {
    var min, max;
    if (arguments.length === 2) {
        min = 0;
        max = arguments[1];
    } else if (arguments.length === 3) {
        min = arguments[1];
        max = arguments[2];
    } else {
        throw new Error('wrong number of arguments');
    }
    while (value >= max) value -= (max - min);
    while (value < min) value += (max - min);
    return value;
};

Boid.clamp = function(value, limit) {
    return Math.min(limit, Math.max(-limit, value));
};

Boid.meanAngle = function() {
    var sumx = 0, sumy = 0, len = arguments.length;
    for (var i = 0; i < len; i++) {
        sumx += Math.cos(arguments[i]);
        sumy += Math.sin(arguments[i]);
    }
    return Math.atan2(sumy / len, sumx / len);
};

Boid.prototype.checkObstacle = function(){
    var obstacle = obstacleInstance;
    var dist = this.distance(obstacleInstance, w, h)
    if (dist < this.vision) {
        var angleFromBoid = this.angle(boid, w, h)
        var mindist = this.radius * 2;
        if(dist < mindist || angleFromBoid < boidFOVdeg){
            if(neighbors.length >= neighbourAmt){
                var lastTracked = neighbors[neighbourAmt-1];
                if(dist < lastTracked.d){
                    neighbors[neighbourAmt-1] = {d:dist,b:boid};
                }
            }else{
                neighbors.push({d:dist,b:boid});
                neighbors.sort((a,b)=>{a.d>b.d});
            }
        }
    }
}

Boid.prototype.step = function(swarm) {
    var w = swarm.width, h = swarm.height;
    var neighbors = this.getNeighbors(swarm);
    //if(stopSim)return
    if (neighbors.length > 0) {
        var meanhx = 0, meanhy = 0;
        var meanx = 0, meany = 0;
        var mindist = this.radius, min = null;
        var sepdist = boidSepDist;
        var sepx = 0, sepy = 0;
        var cohNum = 0, sepNum = 0;

        var target;

        for (var i = 0; i < neighbors.length; i++) {
            var boid = neighbors[i];

            if(Obstacle.prototype.isPrototypeOf(boid) && boid.enabled){
                //console.log("uh")
                target = Math.atan2(this.y - boid.y, this.x - boid.x);
                //mindist = -Infinity;
                //min = boid;
                break;
            }else{
                meanhx += Math.cos(boid.heading); // Average Heading
                meanhy += Math.sin(boid.heading);
                //meanx += boid.x; // Average Position
                //meany += boid.y;
                var dist = this.distance(boid, w, h);
    
                if (dist < sepdist){
                    sepx += boid.x;
                    sepy += boid.y;
                    sepNum++;
                }else{
                    meanx += boid.x; // Average Position
                    meany += boid.y;
                    cohNum++;
                }
    
                if (dist < mindist) {
                    mindist = dist;
                    min = boid;
                }
            }

            
            
        }
        if(target==undefined){

            meanhx /= neighbors.length;
            meanhy /= neighbors.length;
            /*
            if(cohNum!=0){
                meanx /= cohNum;
                meany /= cohNum;
            }
            if(sepNum!=0){
                sepx /= sepNum;
                sepy /= sepNum;
            }
            */

            //meanx += sepx*sepVsCohWeight;
            //meany += sepy*sepVsCohWeight;
            meanx = (meanx+cohW,sepx*sepW)/(neighbors.length)
            meany = (meany*cohW+sepy*sepW)/(neighbors.length)
            
            if (min) {
                // Keep away!
                target = Math.atan2(this.y - min.y, this.x - min.x);
            } else {
                
                // Match heading and move towards center
                var meanh = Math.atan2(meanhy, meanhx);
                var center = Math.atan2(meany - this.y, meanx - this.x);
                
                target = Boid.meanAngle(meanh, meanh, meanh, center); // 3:2:1 Alignment:Seperation:Cohesion
            }
        }

        // Move in this direction
        var delta = Boid.wrap(target - this.heading, -Math.PI, Math.PI);
        delta = Boid.clamp(delta, this.radialSpeed);
        this.heading = Boid.wrap(this.heading + delta, -Math.PI, Math.PI);
    }

    this.move(swarm);
};

Boid.prototype.move = function(swarm) {
    var padding = swarm.padding;
    var width = swarm.width, height = swarm.height;
    this.x = Boid.wrap(this.x + Math.cos(this.heading) * this.speed,
                       -padding, width + padding * 2);
    this.y = Boid.wrap(this.y + Math.sin(this.heading) * this.speed,
                       -padding, height + padding * 2);
};

/* Swarm prototype. */

function Swarm(ctx) {
    this.ctx = ctx;
    this.boids = [];
    var swarm = this;
    this.animate = function() {
        Swarm.step(swarm);
    };
    this.padding = 0;
}

Swarm.prototype.createBoid = function(n, uniformRot = false, uniformPos = false, randomScale = 2, regionRadius = 1*meterScale) {
    obstacleInstance.x = regionRadius*2 + distFromObstacle
    
    var goldenRatio = (1 + Math.sqrt(5)) / 2;
    var angleIncrement = Math.PI * 2 * goldenRatio;

    for (var i = 0; i < (n || 1); i++) {
        var newBoid = new Boid(this)
        if(uniformPos){
            var distance = i/n;
            var angle = angleIncrement * i;
            var x = distance*Math.cos(angle)*regionRadius + regionRadius
            var y = distance*Math.sin(angle)*regionRadius + envDimensionsH/2
            newBoid.x = x + Math.random()*randomScale //- Math.random()*randomScale;
            newBoid.y = y + Math.random()*randomScale //- Math.random()*randomScale;
        }
        if(uniformRot){
            newBoid.heading = 0;
        }
        this.boids.push(newBoid);
    }
};

Swarm.prototype.clear = function() {
    this.boids = [];
};

Swarm.prototype.end = function() {
    clearInterval(this.id);
};

Object.defineProperty(Swarm.prototype, 'width', {get: function() {
    return envDimensionsW;this.ctx.canvas.width;
}});

Object.defineProperty(Swarm.prototype, 'height', {get: function() {
    return envDimensionsH;this.ctx.canvas.height;
}});
var result = [];
Swarm.step = function (swarm) {
    if(stopSim)return;
    currentStep++;

    var ctx = swarm.ctx;
    //if (ctx.canvas.width != window.innerWidth)
    //    ctx.canvas.width = window.innerWidth;
    //if (ctx.canvas.height != window.innerHeight)
    //    ctx.canvas.height = window.innerHeight;
    ctx.canvas.width = envDimensionsW;
    ctx.canvas.height = envDimensionsH;
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, swarm.width, swarm.height);

    obstacleInstance.draw(ctx)
    distanceToNearestNeighbour = []
    for (var i = 0; i < swarm.boids.length; i++) {
        ctxLeak = ctx;
        //if(!stopSim)
            swarm.boids[i].step(swarm);
        swarm.boids[i].draw(ctx);
    }

    if(currentStep== pauseStep){
        var cc = getConnectedComponents(swarm,true);
        swarm.end()
        currentStep = 0;

        if(connectedComponentTrackThing[cc] == undefined){
            connectedComponentTrackThing[cc] = 0;
        }
        connectedComponentTrackThing[cc]++;
        connectedComponentTrackThing_n--
        if(connectedComponentTrackThing_n < 0){
            result = connectedComponentTrackThing;
            var keys = Object.keys(result);
            
            var n = Math.max(...keys)
            var arr = []
            for (let i = 0; i < n; i++) {
                if(result[i] == undefined){
                    arr.push(0)
                }else{
                    arr.push(result[i])
                }
            }
            exportToCsv(arr);
            return arr;
        }
        console.log(cc);
        
        startNewSwarm()
    }
};

/* Test */



var swarm; // defined globally for skewer
//document.addEventListener('DOMContentLoaded', function() {
function startNewSwarm(){
    swarm = new Swarm(document.getElementById('canvas').getContext('2d'));
    swarm.id = setInterval(swarm.animate, stepInterval);
    swarm.animate();
    swarm.clear();
    swarm.createBoid(boidCount, true, true, randness, 1*meterScale);
}
//});

//https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
function hsl2rgb(h,s,l) 
{
   let a=s*Math.min(l,1-l);
   let f= (n,k=(n+h/30)%12) => (l - a*Math.max(Math.min(k-3,9-k,1),-1))*255;
   return [f(0),f(8),f(4)];
}

//https://stackoverflow.com/questions/45611674/export-2d-javascript-array-to-excel-sheet
exportToCsv = function(Results) {
    var CsvString = "";
    Results.forEach(function(RowItem, RowIndex) {
        if(!Array.prototype.isPrototypeOf(RowItem)){
            RowItem = [RowItem];
        }
        RowItem.forEach(function(ColItem, ColIndex) {
            CsvString += ColItem + ',';
        });
        CsvString += "\r\n";
    });
    CsvString = "data:application/csv," + encodeURIComponent(CsvString);
   var x = document.createElement("A");
   x.setAttribute("href", CsvString );
   x.setAttribute("download","somedata.csv");
   document.body.appendChild(x);
   x.click();
  }